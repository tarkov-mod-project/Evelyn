"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigTypes_1 = require("C:/snapshot/project/obj/models/enums/ConfigTypes");
const Traders_1 = require("C:/snapshot/project/obj/models/enums/Traders");
// New trader settings
const baseJson = __importStar(require("../db/base.json"));
const assortJson = __importStar(require("../db/assort.json"));
class SampleTrader {
    mod;
    logger;
    configServer;
    ragfairConfig;
    constructor() {
        this.mod = "Evelyn"; // Set name of mod so we can log it to console later
    }
    /**
     * Some work needs to be done prior to SPT code being loaded, registering the profile image + setting trader update time inside the trader config json
     * @param container Dependency container
     */
    preAkiLoad(container) {
        this.logger = container.resolve("WinstonLogger");
        this.logger.debug(`[${this.mod}] preAki Loading... `);
        const preAkiModLoader = container.resolve("PreAkiModLoader");
        const imageRouter = container.resolve("ImageRouter");
        const configServer = container.resolve("ConfigServer");
        const traderConfig = configServer.getConfig(ConfigTypes_1.ConfigTypes.TRADER);
        this.registerProfileImage(preAkiModLoader, imageRouter);
        Traders_1.Traders[baseJson._id] = baseJson._id;
        this.setupTraderUpdateTime(traderConfig);
        this.logger.debug(`[${this.mod}] preAki Loaded`);
    }
    postAkiLoad(container) {
        // get logger
        const logger = container.resolve("WinstonLogger");
        // get the config server
        const configServer = container.resolve("ConfigServer");
        // Request quest config
        // Required - ConfigTypes.QUEST is the enum of the config we want
        const questConfig = configServer.getConfig(ConfigTypes_1.ConfigTypes.QUEST);
        // BEAR & USEC specific Quest-Side id
        questConfig.usecOnlyQuests = [
            "6179b5eabca27a099552e052",
            "5e383a6386f77465910ce1f3",
            "5e4d515e86f77438b2195244",
            "639282134ed9512be67647ed",
            "TheWestPart1",
            "BattleReady",
            "TheWestPart2",
            "UnexpectedMeeting",
            "UnknownTransmission",
            "TheLittleCrew",
            "Cleanout",
            "WelcomeGift",
            "Retribution",
            "TheWestPart3"
        ];
        questConfig.bearOnlyQuests = [
            "6179b5b06e9dd54ac275e409",
            "5e381b0286f77420e3417a74",
            "5e4d4ac186f774264f758336",
            "639136d68ba6894d155e77cf",
            "AccessoriesExtravaganza",
            "MemberOfMI6",
            "RoyalLoyalty",
            "Hum?",
            "BusinessIsBusiness",
            "InformationGathering",
            "AKS-uMastery",
            "Fortifying",
            "StressConditioning",
            "GearedUpForTheTerrain",
            "LackOfComponents",
            "ForTheMotherOfRussia",
            "WantedTagilla",
            "WantedShturman",
            "DefencePerimeter",
            "BlackFriday",
            "BackToOurLand",
            "WantedReshala",
            "NightInvestigation",
            "WantedSanitar",
            "PirojkisOutdoors",
            "PromotionJuniorLieutenant",
            "PromotionLieutenant",
            "PromotionSeniorLieutenant",
            "PromotionCaptain",
            "PromotionMajor",
            "PromotionLieutenantColonel",
            "PromotionColonel",
            "PromotionMajorGeneral"
        ];
        // log the file change
        //logger.info(`old "usecOnlyQuests": ${questConfig.usecOnlyQuests}`)    
        //logger.info(`old "bearOnlyQuests": ${questConfig.bearOnlyQuests}`)
        //logger.info(`new "usecOnlyQuests": ${questConfig.usecOnlyQuests}`)
        //logger.info(`new "bearOnlyQuests": ${questConfig.bearOnlyQuests}`)
    }
    /**
     * Majority of trader-related work occurs after the aki database has been loaded but prior to SPT code being run
     * @param container Dependency container
     */
    postDBLoad(container) {
        this.logger.debug(`[${this.mod}] postDb Loading... `);
        this.configServer = container.resolve("ConfigServer");
        this.ragfairConfig = this.configServer.getConfig(ConfigTypes_1.ConfigTypes.RAGFAIR);
        // Resolve SPT classes we'll use
        const databaseServer = container.resolve("DatabaseServer");
        const configServer = container.resolve("ConfigServer");
        const traderConfig = configServer.getConfig(ConfigTypes_1.ConfigTypes.TRADER);
        const jsonUtil = container.resolve("JsonUtil");
        // Get a reference to the database tables
        const tables = databaseServer.getTables();
        // Add new trader to the trader dictionary in DatabaseServer
        this.addTraderToDb(baseJson, tables, jsonUtil);
        this.addTraderToLocales(tables, baseJson.name, "Evelyn", baseJson.nickname, baseJson.location, "A trained American soldier who leads a small USEC group and seeks to join the High Command at the Military Base.");
        // Add item purchase threshold value (what % durability does trader stop buying items at)
        //        traderConfig.durabilityPurchaseThreshhold[baseJson._id] = 60;
        this.ragfairConfig.traders[baseJson._id] = true;
        this.logger.debug(`[${this.mod}] postDb Loaded`);
    }
    /**
     * Add profile picture to our trader
     * @param preAkiModLoader mod loader class - used to get the mods file path
     * @param imageRouter image router class - used to register the trader image path so we see their image on trader page
     */
    registerProfileImage(preAkiModLoader, imageRouter) {
        // Reference the mod "res" folder
        const imageFilepath = `./${preAkiModLoader.getModPath(this.mod)}res`;
        // Register a route to point to the profile picture
        imageRouter.addRoute(baseJson.avatar.replace(".jpg", ""), `${imageFilepath}/Evelyn.jpg`);
    }
    /**
     * Add record to trader config to set the refresh time of trader in seconds (default is 60 minutes)
     * @param traderConfig trader config to add our trader to
     */
    setupTraderUpdateTime(traderConfig) {
        // Add refresh time in seconds to config
        const traderRefreshRecord = { traderId: baseJson._id, seconds: 3600 };
        traderConfig.updateTime.push(traderRefreshRecord);
    }
    /**
     * Add our new trader to the database
     * @param traderDetailsToAdd trader details
     * @param tables database
     * @param jsonUtil json utility class
     */
    addTraderToDb(Evelyn, tables, jsonUtil) {
        tables.traders[Evelyn._id] = {
            assort: jsonUtil.deserialize(jsonUtil.serialize(assortJson)),
            base: jsonUtil.deserialize(jsonUtil.serialize(Evelyn)),
            questassort: {
                started: {},
                success: {
                    "6577bf95c9b1f9d904dadade": "TheLittleCrew",
                    "47f669ab803c025d0e9bpupu": "TheLittleCrew",
                    "96ocuidcon2q62tndagyat69": "TheWestPart1",
                    "9a697338df84xaxa6c219f5c": "TheWestPart1",
                    "eaza16427540f0a28d0acb90": "TheWestPart1",
                    "wazp53d5f67c52f55ca0cd84": "BattleReady",
                    "a75dd0e9f3119a3696b8deck": "BattleReady",
                    "yat6942c099f83522bcf1aa2": "BattleReady",
                    "3dba6127e876d75e5798bvbv": "TheWestPart2",
                    "flo5369a11ad8dc828348f41": "Cleanout",
                    "13dead58646c7dbe20096acq": "Cleanout",
                    "53ab150266d9a475038blose": "WelcomeGift",
                    "hgo72351f2a2aefebc5ed960": "WelcomeGift",
                    "13dead58646c7dbe2009662a": "TheWestPart3",
                    "13dead58646c7dbe2009662s": "TheWestPart3"
                },
                fail: {}
            }
        };
    }
    /**
     * Add traders name/location/description to the locale table
     * @param tables database tables
     * @param fullName fullname of trader
     * @param firstName first name of trader
     * @param nickName nickname of trader
     * @param location location of trader
     * @param description description of trader
     */
    addTraderToLocales(tables, fullName, firstName, nickName, location, description) {
        // For each language, add locale for the new trader
        const locales = Object.values(tables.locales.global);
        for (const locale of locales) {
            locale[`${baseJson._id} FullName`] = fullName;
            locale[`${baseJson._id} FirstName`] = firstName;
            locale[`${baseJson._id} Nickname`] = nickName;
            locale[`${baseJson._id} Location`] = location;
            locale[`${baseJson._id} Description`] = description;
        }
    }
    addItemToLocales(tables, itemTpl, name, shortName, Description) {
        // For each language, add locale for the new trader
        const locales = Object.values(tables.locales.global);
        for (const locale of locales) {
            locale[`${itemTpl} Name`] = name;
            locale[`${itemTpl} ShortName`] = shortName;
            locale[`${itemTpl} Description`] = Description;
        }
    }
}
module.exports = { mod: new SampleTrader() };
//# sourceMappingURL=mod.js.map